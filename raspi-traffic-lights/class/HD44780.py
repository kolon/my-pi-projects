import time
import RPi.GPIO as GPIO
# Assumes using BCM pin mode
# Assumes the following pin layout:
#
# LCD pins VSS, RW, K to GND
#          VDD to 5V
#          A to 5v using resistor, min 1k ohm (backlight LED brightness) 
#          RS to BCM GPIO 7
#          E to BCM GPIO 8
#          D4-7 to BCM GPIO 25, 24, 23, 18
#          D0-3 N/C
#
# This all connects to one side of the RasPi GPIO pins, so the other side can be used freely
#

class HD44780:
    def __init__(self, pin_rs=7, pin_e=8, pins_db=[25, 24, 23, 18]):
        self.pin_rs=pin_rs
        self.pin_e=pin_e
        self.pins_db=pins_db
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin_e, GPIO.OUT)
        GPIO.setup(self.pin_rs, GPIO.OUT)
        for pin in self.pins_db:
            GPIO.setup(pin, GPIO.OUT)
        self.clear()

    def clear(self):
        """ Blank / Reset LCD """
        self.cmd(0x33) # $33 8-bit mode
        self.cmd(0x32) # $32 8-bit mode
        self.cmd(0x28) # $28 8-bit mode
        self.cmd(0x0C) # $0C 8-bit mode
        self.cmd(0x06) # $06 8-bit mode
        self.cmd(0x01) # $01 8-bit mode
 
    def cmd(self, bits, char_mode=False):
        """ Send command to LCD """
        time.sleep(0.001)
        bits=bin(bits)[2:].zfill(8)
        GPIO.output(self.pin_rs, char_mode)
        for pin in self.pins_db:
            GPIO.output(pin, False)
        for i in range(4):
            if bits[i] == "1":
                GPIO.output(self.pins_db[::-1][i], True)
        GPIO.output(self.pin_e, True)
        GPIO.output(self.pin_e, False)
        for pin in self.pins_db:
            GPIO.output(pin, False)
        for i in range(4,8):
            if bits[i] == "1":
                GPIO.output(self.pins_db[::-1][i-4], True)
        GPIO.output(self.pin_e, True)
        GPIO.output(self.pin_e, False)

    def message(self, text):
        """ Send string to LCD. Newline wraps to second line"""
        self.cmd(0x80)        # goto line 1
        line = 1
        for char in text:
            if char == '\n':
                line = line + 1
                if line = 2:
                    self.cmd(0xC0) # next line
                if line = 3:
                    self.cmd(0x94) # next line
                if line = 4:
                    self.cmd(0xD4) # next line
            else:
                self.cmd(ord(char),True)

    def messageline(self, text, line)
        if line = 1:
            self.cmd(0x80) # next line
        if line = 2:
            self.cmd(0xC0) # next line
        if line = 3:
            self.cmd(0x94) # next line
        if line = 4:
            self.cmd(0xD4) # next line
        self.cmd(ord(char),True)