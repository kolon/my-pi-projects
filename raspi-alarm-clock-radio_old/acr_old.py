#  Alarm Clock Radio
#
# John Wood, v0.1
#
# Required:
#  Speaker in 3.5mm jack
#  

try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print("Error importing RPi.GPIO!  This is probably because you need superuser privileges.  You can achieve this by using 'sudo' to run your script")
import time
import sys
import smbus

sys.path.append("class")
from HD44780 import HD44780

#define device array

def piIO.init():
    GPIO.setmode(GPIO.BOARD)
    #find rasp revision
    if GPIO.REVISION == 1:
        P1.I2C = smbus.SMBus(1)
        P5.I2C = -1
    elif GPIO.REVISION == 2:
        P1.I2C = smbus.SMBus(1)
        P1.I2C = smbus.SMBus(0) 
    else:
        print("Unknown version")

def piIO.adddevice(id,address,gpio register,):
    #create array for device
    

def piIO.inout(bus, address, pin, direction):
    if bus == "pi":
        if address == "P1":
            GPIO.setup(pin, direction)
        elif address == "P5":
            ?
    elif bus == P1.I2C:
        P1.I2C(address, pin, 

def piIO.send:
    pass

def 
        



DEBUG = 1

# use P1 header pin numbering convention
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

PIN_LED_RED = 4
PIN_LED_YEL = 17
PIN_LED_GRN = 22

PIN_BUT_PEDX = 9		# Pedestrian crossing button
PIN_LED_PEDX_WAIT = 27		# pedestrian "wait" light
PIN_LED_PEDX_GRN = 2		# pedestrian walk light
PIN_LED_PEDX_RED = 3		# pedestrian stop light

PIN_BUT_STOP = 10		# stop processing button




# Set up the GPIO channels - one input and one output
GPIO.setup(PIN_LED_RED, GPIO.OUT)
GPIO.setup(PIN_LED_YEL, GPIO.OUT)
GPIO.setup(PIN_LED_GRN, GPIO.OUT)

GPIO.setup(PIN_BUT_PEDX, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(PIN_LED_PEDX_WAIT, GPIO.OUT)
GPIO.setup(PIN_LED_PEDX_GRN, GPIO.OUT)
GPIO.setup(PIN_LED_PEDX_RED, GPIO.OUT)

GPIO.setup(PIN_BUT_STOP, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

if DEBUG == 1:
    print "Done setup"

lcd = HD44780()

#Traffic light routine
def turn_red():		# only green should be on
    if DEBUG == 1:
        print "Turning RED"
    GPIO.output(PIN_LED_GRN,0)
    GPIO.output(PIN_LED_YEL,1)    
    time.sleep(3)
    GPIO.output(PIN_LED_YEL,0)
    GPIO.output(PIN_LED_RED,1)

    GPIO.output(PIN_LED_PEDX_RED,0)
    GPIO.output(PIN_LED_PEDX_GRN,1)

def turn_green():	# only red should be on
    if DEBUG == 1:
        print "Turning Green"

    GPIO.output(PIN_LED_PEDX_RED,1)
    GPIO.output(PIN_LED_PEDX_GRN,0)

    GPIO.output(PIN_LED_RED,0)
    flash_yellow()
    GPIO.output(PIN_LED_GRN,1)


def flash_yellow():
    if DEBUG == 1:
        print "Flashing Yellow"
    for i in range(6):
        GPIO.output(PIN_LED_YEL,1)
        time.sleep(.75)
        GPIO.output(PIN_LED_YEL,0)
        time.sleep(.5)
   
    GPIO.output(PIN_LED_YEL,1)
    time.sleep(.75)
    GPIO.output(PIN_LED_YEL,0)

def main():
    GPIO.output(PIN_LED_GRN,1)

    lcd.clear()
    lcd.message("   Push button\n    to cross")
    while GPIO.input(PIN_BUT_STOP) == GPIO.LOW:

        time.sleep(0.1)

        #check input
        if GPIO.input(PIN_BUT_PEDX) == GPIO.LOW:
#            if DEBUG == 1:
#                print "Continue"
            continue

        #if we get here then pedestrian button pushed
        if DEBUG == 1:
            print "Pedestrian button pushed"
        
        lcd.clear()
        lcd.message("      Please\n      Wait")
        GPIO.output(PIN_LED_PEDX_WAIT,1)
        time.sleep(5)
        turn_red()
        
        GPIO.output(PIN_LED_PEDX_WAIT,0)
        lcd.clear()
        lcd.message("     Look left\n  when crossing")

        time.sleep(10)
        lcd.clear()
        lcd.message("    Do not cross")
        turn_green()

        lcd.clear()
        lcd.message("   Push button\n    to cross")


main()
if DEBUG == 1:
    print "Stopping"
GPIO.cleanup()

#End
