import hd44780
from .backends import MCP230xxBackend
#from .inputs import MCP230xxInput

PINMAP = {
        'RS': 2,
        'RW': 3,
        'E': 4,
        'D4': 3,
        'D5': 4,
        'D6': 5,
        'D7': 6,
        'LED': 7,
}

CHARMAP = {
        0: (
                0b01110,
                0b10001,
                0b10111,
                0b11001,
                0b10111,
                0b10001,
                0b01110,
                0b00000,
        ),
        1: (
                0b10010,
                0b00100,
                0b01001,
                0b10010,
                0b00100,
                0b01001,
                0b10010,
                0b00100,
        ),
        2: (
                0b01001,
                0b00100,
                0b10010,
                0b01001,
                0b00100,
                0b10010,
                0b01001,
                0b00100,
        ),
        3: (
                0b10101,
                0b01010,
                0b10101,
                0b01010,
                0b10101,
                0b01010,
                0b10101,
                0b01010,
        ),
        4: (
                0b01010,
                0b10101,
                0b01010,
                0b10101,
                0b01010,
                0b10101,
                0b01010,
                0b10101,
        ),
        5: (
                0b11111,
                0b11111,
                0b11111,
                0b11111,
                0b11111,
                0b11111,
                0b11111,
                0b11111,
        ),
        6: (
                0b11111,
                0b10001,
                0b10101,
                0b10101,
                0b10101,
                0b10101,
                0b10001,
                0b11111,
        ),
        7: (
                0b00000,
                0b01110,
                0b01010,
                0b01010,
                0b01010,
                0b01010,
                0b01110,
                0b00000,
        ),
}


display = pylcd.hd44780.Display(backend = pylcd.MCP230xxBackend, pinmap = PINMAP, charmap = CHARMAP, lines = 2, columns = 16, skip_init = args.skip_init, debug = False)
display.set_display_enable(cursor = false, cursor_blink = false)
display.clear()
display.home()
ui = pylcd.hd44780.DisplayUI(display, pylcd.SystemInput, debug = True)
ui.message("This is an LCD test", align = 'center', wrap = False, duration = 2.5)



