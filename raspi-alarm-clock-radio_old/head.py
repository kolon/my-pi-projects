try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print("Error importing RPi.GPIO!  This is probably because you need superuser privileges.  You can achieve this by using 'sudo' to run your script")
import time
import sys
import thread

from AdaFruit_MCP230xx import Adafruit_MCP230xx
from Adafruit_CharLCD import Adafruit_CharLCD

