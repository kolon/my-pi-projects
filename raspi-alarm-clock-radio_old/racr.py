#  Alarm Clock Radio
#
import sys
import head

def racr_setup():
    mcp1 = Adafruit_MCP230XX(address = 0x20, num_gpios = 16)
    mcp2 = Adafruit_MCP230XX(address = 0x21, num_gpios = 16)

    #led time display
    mcp2.config(0, mcp.OUTPUT)  #anode 1   - side a
    mcp2.config(1, mcp.OUTPUT)  #anode 2
    mcp2.config(2, mcp.OUTPUT)  #anode 3
    mcp2.config(3, mcp.OUTPUT)  #anode 4
    mcp2.config(4, mcp.OUTPUT)  #anode 5
    mcp2.config(5, mcp.OUTPUT)  #anode 6
    mcp2.config(6, mcp.OUTPUT)  #anode 7
    mcp2.config(7, mcp.OUTPUT)  #anode 8
    mcp2.config(8, mcp.OUTPUT)  #anode 9   - side b
    mcp2.config(9, mcp.OUTPUT)  #anode 10
    mcp2.config(10, mcp.OUTPUT) #anode 11
    mcp2.config(11, mcp.OUTPUT) #anode 12
    mcp2.config(12, mcp.OUTPUT) #anode 13
    mcp2.config(13, mcp.OUTPUT) #anode 14
    mcp2.config(14, mcp.OUTPUT) #anode 15 
    mcp2.config(15, mcp.OUTPUT) #anode 16
    mcp1.config(13, mcp.OUTPUT) #anode 17 - am/pm dot?
    mcp1.config(14, mcp.OUTPUT) #cathode 1
    mcp1.config(15, mcp.OUTPUT) #cathode 2
    mcp1.config(16, mcp.OUTPUT) #cathode 3
    
    #RTC setup
    
    #lcd display setup
    mcp1.config(0, mcp.OUTPUT)  #reset
    mcp1.config(1, mcp.OUTPUT)  #E
    mcp1.config(2, mcp.OUTPUT)  #4
    mcp1.config(3, mcp.OUTPUT)  #5
    mcp1.config(4, mcp.OUTPUT)  #6
    mcp1.config(5, mcp.OUTPUT)  #7
    
    
def thread_time():
    while true:
        time.sleep(.5)
        #get time
        #update time
        #alternate colon flashing

def main():
    #start time thread
    thread.start_new_thread(thread_time())
    
    #start info/menu thread
    thread.start_new_thread(thread_lcd())
    
    #let the threads do all the work, while we now do nothing
    while true:
        time.sleep(10)

setup()
main()