
#import python modules
import threading
import logging
import ConfigParser
from time import sleep 

#import acr modules
from acr_dht11 import acr_dht11
from acr_bmp180 import acr_bmp180
from acr_lcd import acr_lcd
from acr_sevensegment import acr_sevensegment

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%threadName)-10s) %message)s',)
Config = ConfigParser.ConfigParser()
Config.read("acr.cfg")

#setup threads (old way)
#dht11_thread = threading.Thread(target=acr_dht11)
#bmp180_thread = threading.Thread(name="acr_bmp180",target=acr_bmp180)
#lcd_thread = threading.Thread(target=acr_lcd)
#sevensegment_thread = threading.Thread(target=acr_sevensegment)

#start threads
#dht11_thread.start()
#bmp180_thread.start()
#lcd_thread.start()
#sevensegment_thread.start()
threadstostart = []
#if Config.getboolean("dht11","enabled"):
#    threadstostart.append(ConfigSectionMap("dht11")['name']))
#if Config.getboolean("bmp180","enabled"):
#    threadstostart.append(ConfigSectionMap("bmp180")['name']))
#if Config.getboolean("lcd","enabled"):
#    threadstostart.append(ConfigSectionMap("lcd")['name']))
#if Config.getboolean("sevensegment","enabled"):
#    threadstostart.append(ConfigSectionMap("sevensegment")['name']))

threadstostart = ['acr_dht11','acr_bmp180','acr_lcd','acr_sevensegment']

while True:
    #get missing threads
    threadsrunning = []
    for name in threading.enumerate():      #for each thread running
        threadname = str(name)              #convert thread object reference to string
        if "MainThread" in threadname:      #exclude main thread
            continue
        i = threadname.find("(") + 1        #extract thread name
        j = threadname.find(",")            #this will need to change if Python changes format
        threadname = threadname[i:j]        
        threadsrunning.append(threadname)   #add running thread to list
    
    threadsnotrunning = list(set(threadstostart) - set(threadsrunning)) #calculate list of threads not running
    
    for threadname in threadsnotrunning:
        threadtostart = globals()[threadname]                           # set missing thread
        thread = threading.Thread(name=threadname,target=threadtostart) # set threading object
        thread.start()                                                  # start thread
        
    sleep(10)  #do nothing for a bit


#configparser helper function
def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1
