# Alarm Clock Radio - a comprehensive alarm clock radio,
#                     written in Python for the Raspberry Pi
# Copyright 2015 John Wood (john@kolon.co.uk)
#
# 16x2 i2c LCD display
#   This is run as a thread from the main application
#

import acr_lcdi2c 
import sqlite3 as sqlite
import RPi.GPIO as GPIO
from threading import Timer
from time import sleep
from datetime import datetime

backlighttimeout = 10

def LCDBacklightOff():
    #print "LCD Backlight Off"
    lcd.backlight(0)

def IRSensorChange(pin):
    if (GPIO.input(18) == 1):
        IRMovement = True
        
        global t
        t.cancel()  #stop the timer
        t = Timer(backlighttimeout, LCDBacklightOff)
        #t.start()   #start the timer again
        
        lcd.backlight(1)
    else:
        IRMovement = False
    #print "change {0}".format(IRMovement)

t = Timer(backlighttimeout, LCDBacklightOff)
#t.start()

def acr_lcd():
    #variable defines
    firstrun = True
    IRMovement = False
    degree = [ [0x06,0xa9,0xa9,0x06,0x00,0x00,0x00,0x00], ]  # custom "degree" symbol
    lasttimedetail = ""
    lasttimeclock = ""
    
    #Setup
    global lcd
    lcd = acr_lcdi2c.lcd(0x27,1)
    lcd.lcd_clear()
    lcd.lcd_load_custom_chars(degree)
    lcd.backlight(1)
    
    #custom backlight control
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(18,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(18,GPIO.BOTH, callback=IRSensorChange)
    
    lcd.lcd_display_string_pos("{0}C".format(chr(0)),1,4)	# degrees C
    lcd.lcd_display_string_pos("Pa",2,7)
    lcd.lcd_display_string_pos("%HA",2,13)

    #Main program
    while True:
        if (firstrun == False) :
            sleep(1)
            # or movement detected?  use pwm on 18?
        firstrun = False
        # get values from DB (only temperature so far)
        # only really need to do this every 60 seconds or so...
        try:
            con = sqlite.connect('acr.db')
            cur = con.cursor()
            cur.execute("SELECT data1 FROM data WHERE type='temp' ORDER BY ID DESC LIMIT 1")  # get last temperature recorded
            temprow = cur.fetchone()
            cur.execute("SELECT data1 FROM data WHERE type='pres' ORDER BY ID DESC LIMIT 1")  # get last pressure recorded
            presrow = cur.fetchone()
            cur.execute("SELECT data1 FROM data WHERE type='humidity' ORDER BY ID DESC LIMIT 1")  # get last pressure recorded
            humidityrow = cur.fetchone()
            con.close()
            
            if temprow != None :
                lcd.lcd_display_string_pos("{0}".format(temprow[0]),1,0)
            # convert temp to F K
            if presrow != None :
                lcd.lcd_display_string_pos("{0}".format(presrow[0]),2,0)
            #convert Pa to atm, mbar, lb/in2, psi etc.
            if humidityrow != None :
                lcd.lcd_display_string_pos("{0}".format(humidityrow[0]),2,11)
            #convert %HA to dewpoint, saturation etc.
        except sqlite.Error, e:
            print "Error {0}".format(e.args[0])
            #log error
            continue
        finally:
            if (con) : con.close()

if __name__ == '__main__':
    acr_lcd()