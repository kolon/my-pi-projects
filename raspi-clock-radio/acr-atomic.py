# Alarm Clock Radio - a comprehensive alarm clock radio,
#                     written in Python for the Raspberry Pi
# Copyright 2015 John Wood (john@kolon.co.uk)
#
# Generic MSF Atomic Clock Receiver
#   This is run as a thread from the main application
#
# Bugs: Ideally, it should collect a minute's worth of data and
#       process that, rather than trying to interpret the data
#       on the fly.
#
#       Also, this doesn't reset when there are two minute start
#       markers in the same minute.  This would indicate that
#       there is interference.
#

#import sys
import RPi.GPIO as GPIO
import time
from datetime import datetime
from bitstring import Bits,BitArray
import calendar

def getcode(ignorezeros):
    n = []
    
    #wait for next 1
    if (ignorezeros == True):
        while (GPIO.input(24) == 0):
            time.sleep(0.01)
            continue
    
    for x in range(0,9):
        n.append(GPIO.input(24))
        time.sleep(0.1)
    
    #print "{0} {1}".format(datetime.now().time(),n)
    return n

def getparity(code):
    parity = 0
    for bit in Bits(bytes=code):
        parity ^= int(bit)
    return parity

def acr_atomic():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    data = []
    second = []
    print datetime.now().time()
    
    #getparity(bin(''.join(map(daycheck))))
#    a = ''.join(map(str, [1,1,1,1,1,1,1]))
#    b = BitArray(bin=a)
#    print getparity(b)
#    import sys
#    sys.exit()

    while True:
        time.sleep(0.1)
        y = GPIO.input(24)
        data.append(y)
        
        #look for the minute marker (0.5 secs on, 0.5 secs off)
        if (data != [1,1,1,1,1,0,0,0,0,0]):
            while (len(data) >= 10):
                del data[0]
            continue
        print "{0} minute {1}".format(datetime.now().time(),data)
        del data[0]
        
        #get + DUT1 offset
        dutoffsetplus = 0
        for a in range(0,8):
            i = getcode(True)
            if (i[2] == 1):
                dutoffsetplus += a
        print "{0} dutoffsetplus (8 secs) {1}".format(datetime.now().time(),dutoffsetplus)

        #get - DUT1 offset
        dutoffsetneg = 0
        for a in range(0,8):
            i = getcode(True)
            if (i[2] == 1):
                dutoffsetneg += (a * -1)
        print "{0} dutoffsetneg (16 secs) {1}".format(datetime.now().time(),dutoffsetneg)
        
        #get year
        year = 0
        yearcheck = []
        for a in range(0,8):
            i = getcode(True)
            #if (a == 0) & (i = [1,0,0]:     #leap second inserted between 16-17 seconds, 16 second marker removed if negative
            #    print "{0} leap second (24 secs) {1}".format(datetime.now().time(),i)
            #    i = getcode(True,3)
            yearcheck.append(i[2])
            if (i[2] == 1):
                if (a == 0):
                    year += 80
                elif (a == 1):
                    year += 40
                elif (a == 2):
                    year += 20
                elif (a == 3):
                    year += 10
                elif (a == 4):
                    year += 8
                elif (a == 5):
                    year += 4
                elif (a == 6):
                    year += 2
                elif (a == 7):
                    year += 1
        if (year < 15) | (year > 50):        #sanity check
            print "{0} year (24 secs) {1} (incorrect)".format(datetime.now().time(),year)
            year = -1
        else:
            print "{0} year (24 secs) {1}".format(datetime.now().time(),year)
        
        #get month
        month = 0
        for a in range(0,5):
            i = getcode(True)
            if (i[2] == 1):
                if (a == 0):
                    month += 10
                elif (a == 1):
                    month += 8
                elif (a == 2):
                    month += 4
                elif (a == 3):
                    month += 2
                elif (a == 4):
                    month += 1
        if (month < 1) | (month > 12):        #sanity check
            print "{0} month (29 secs) {1} (incorrect)".format(datetime.now().time(),month)
            month = -1
        else:
            print "{0} month (29 secs) {1}".format(datetime.now().time(),month)

        #get day
        day = 0
        daycheck = []
        for a in range(0,6):
            i = getcode(True)
            daycheck.append(i[2])
            if (i[2] == 1):
                if (a == 0):
                    day += 20
                elif (a == 1):
                    day += 10
                elif (a == 2):
                    day += 8
                elif (a == 3):
                    day += 4
                elif (a == 4):
                    day += 2
                elif (a == 5):
                    day += 1
        try:
            if (day < 1) | (day > calendar.monthrange(year, month)[1]) :      # sanity check
                print "{0} day (35 secs) {1} (incorrect)".format(datetime.now().time(),day)
                day = -1
            else:
                print "{0} day (35 secs) {1}".format(datetime.now().time(),day)
        except:
            print "{0} day (35 secs) {1} (unable to verify)".format(datetime.now().time(),day)

        #get day of week
        dow = 0
        dowcheck = []
        for a in range(0,3):
            i = getcode(True)
            dowcheck.append(i[2])
            if (i[2] == 1):
                if (a == 0):
                    dow += 4
                elif (a == 1):
                    dow += 2
                elif (a == 2):
                    dow += 1
        # based on date, what is the current calculated dow?
        try:
            weekday = calendar.weekday(year,month,day) + 1    # python monday = 0
            if (weekday == 7): weekday = 6
    
            if (dow != weekday) :      # sanity check
                print "{0} day of week (38 secs) {1} (incorrect)".format(datetime.now().time(),dow)
                dow = -1
            else:
                print "{0} day of week (38 secs) {1}".format(datetime.now().time(),dow)
        except:
            print "{0} day of week (38 secs) {1} (unable to verify)".format(datetime.now().time(),dow)
        
        timecheck = []
        #get hour
        hour = 0
        for a in range(0,6):
            i = getcode(True)
            timecheck.append(i[2])
            if (i[2] == 1):
                if (a == 0):
                    hour += 20
                elif (a == 1):
                    hour += 10
                elif (a == 2):
                    hour += 8
                elif (a == 3):
                    hour += 4
                elif (a == 4):
                    hour += 2
                elif (a == 5):
                    hour += 1
        if (hour < 0) | (hour > 23):
            print "{0} hour (44 secs) {1} (incorrect)".format(datetime.now().time(),hour)
            hour = -1
        else:
            print "{0} hour (44 secs) {1}".format(datetime.now().time(),hour)

        #get min
        min = 0
        for a in range(0,7):
            i = getcode(True)
            timecheck.append(i[2])
            if (i[2] == 1):
                if (a == 0):
                    min += 40
                elif (a == 1):
                    min += 20
                elif (a == 2):
                    min += 10
                elif (a == 3):
                    min += 8
                elif (a == 4):
                    min += 4
                elif (a == 5):
                    min += 2
                elif (a == 6):
                    min += 1
        if (min < 0) | (min > 59):
            print "{0} min (51 secs) {1} (incorrect)".format(datetime.now().time(),min)
            min = -1
        else:
            print "{0} min (51 secs) {1}".format(datetime.now().time(),min)

        #52 second marker (another minute marker)
        i = getcode(False)
        #if (i == [1,0,0,0,0,0,0,0,0,0]):
        if (i[2] == 0):
            print "{0} 52 second marker pass".format(datetime.now().time(),i)
        else:
            print "{0} 52 second marker failed".format(datetime.now().time(),i)
        
        #Summer time warning (applies to next hour, always on during summertime)
        i = getcode(True)
        #if (i == [0,0,0,0,0,0,0,0,0,0]):
        print "{0} summer time warning {1}".format(datetime.now().time(),i[2])
        
        #year parity
        i = getcode(True)
        #check year parity
        YearParity = getparity(BitArray(bin=''.join(map(str, yearcheck))))
        if (i[2] != YearParity):
            print "{0} Year parity check failed: calculated={1} expected={2}".format(datetime.now().time(),YearParity,i[2])
        else: 
            print "{0} Year parity check passed".format(datetime.now().time(),)
        
        #day parity
        i = getcode(True)
        #check day parity
        DayParity = getparity(BitArray(bin=''.join(map(str,daycheck))))
        if (i[2] != DayParity):
            print "{0} Day parity check failed: calculated={1} expected={2}".format(datetime.now().time(),DayParity,i[2])
        else:
            print "{0} Day parity check passed".format(datetime.now().time(),)
        
        #DOW parity
        i = getcode(True)
        #check DOW parity
        DOWParity = getparity(BitArray(bin=''.join(map(str,dowcheck))))
        if (i[2] != DOWParity):
            print "{0} DOW parity check failed: calculated={1} expected={2}".format(datetime.now().time(),DOWParity,i[2])
        else:
            print "{0} DOW parity check passed".format(datetime.now().time(),)
        
        #time parity
        i = getcode(True)
        #check time parity
        TimeParity = getparity(BitArray(bin=''.join(map(str,timecheck))))
        if (i[2] != TimeParity):
            print "{0} Time parity check failed: calculated={1} expected={2}".format(datetime.now().time(),TimeParity,i[2])
        else:
            print "{0} Time parity check passed".format(datetime.now().time(),)

        #summertime in effect
        i = getcode(True)
        print "{0} summer time {1}".format(datetime.now().time(),i[2])
        
        #59 second marker (always 0)
        i = getcode(False)
        print "{0} 59 second marker {1}".format(datetime.now().time(),i[2])
        
        #sleep(0.09)
        #set hwclock time
        
if __name__ == '__main__':
    acr_atomic()




