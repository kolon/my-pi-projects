# Alarm Clock Radio - a comprehensive alarm clock radio,
#                     written in Python for the Raspberry Pi
# Copyright 2015 John Wood (john@kolon.co.uk)
#
# 7-Segment i2c LED display
#   This is run as a thread from the main application

import time
import datetime
from Adafruit_SevenSegment.SevenSegment import SevenSegment

def acr_sevensegment():
    segment = SevenSegment(address=0x70)

    segment.setBrightness(2)

    while(True):
        now = datetime.datetime.now()
        hour = now.hour
        minute = now.minute
        second = now.second
        # Set hours
        segment.writeDigit(0, int(hour / 10))     # Tens
        segment.writeDigit(1, hour % 10)          # Ones
        # Set minutes
        segment.writeDigit(3, int(minute / 10))   # Tens
        segment.writeDigit(4, minute % 10)        # Ones
        # Toggle color
        segment.setColon(second % 2)              # Toggle colon at 1Hz
        # Wait one second
        time.sleep(1)

if __name__ == '__main__':
    acr_sevensegment()