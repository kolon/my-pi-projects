# Alarm Clock Radio - a comprehensive alarm clock radio,
#                     written in Python for the Raspberry Pi
# Copyright 2015 John Wood (john@kolon.co.uk)
#
# DHT11 Humidity senor recorder
#   This is run as a thread from the main application
#
#  Sample code taken from:
#http://www.uugear.com/portfolio/dht11-humidity-temperature-sensor-module/
#

import RPi.GPIO as GPIO
import sqlite3 as sqlite
import logging
from time import sleep
from datetime import datetime

def bin2dec(string_num):
    return str(int(string_num, 2))

def acr_dht11():
    firstrun = True
    error = False
    
    while True:
        if (firstrun == False):
            if (error == True):
                sleep(2)
                error = False
            else:
                sleep(60)
        else:
            firstrun = False
        
        data = []
        bit_count = 0
        #tmp = 0
        count = 0
        HumidityBit = ""
        TemperatureBit = ""
        crc = ""
    
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(23,GPIO.OUT)
        GPIO.output(23,GPIO.HIGH)
        sleep(0.025)
        GPIO.output(23,GPIO.LOW)
        sleep(0.02)
        GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
        #get 500 readings from sensor
        for i in range(0,500):
            data.append(GPIO.input(23))
    
        try:
            while data[count] == 1:
                #tmp = 1
                count += 1
    
            for i in range(0, 32):
                bit_count = 0
                while data[count] == 0:
                    #tmp = 1
                    count += 1
                while data[count] == 1:
                    bit_count += 1
                    count += 1
                if bit_count > 3:
                    if i>=0 and i<8:
                        HumidityBit += "1"
                    if i>=16 and i<24:
                        TemperatureBit += "1"
                else:
                    if i>=0 and i<8:
                        HumidityBit += "0"
                    if i>=16 and i<24:
                        TemperatureBit += "0"
        except:
            #log collection error
            error = True
            continue
    
        try:
            for i in range(0, 8):
                bit_count = 0
    
                while data[count] == 0:
                    #tmp = 1
                    count = count + 1
    
                while data[count] == 1:
                    bit_count = bit_count + 1
                    count = count + 1
    
                if bit_count > 3:
                    crc = crc + "1"
                else:
                    crc = crc + "0"
        except:
            #log not enough data error
            error = True
            continue
    
        Humidity = bin2dec(HumidityBit)
        Temperature = bin2dec(TemperatureBit)
    
        if int(Humidity) + int(Temperature) - int(bin2dec(crc)) != 0:
            #log crc error
            error = True
            continue
    
        pdate = str(datetime.date(datetime.now()))
        ptime = str(datetime.time(datetime.now()))
    
        try:
            con = sqlite.connect('acr.db')
            cur = con.cursor()
            con.execute("CREATE TABLE IF NOT EXISTS data\
                            (ID INTEGER PRIMARY KEY AUTOINCREMENT,\
                            date		TEXT NOT NULL,\
                            time		TEXT NOT NULL,\
                            type		TEXT NOT NULL,\
                            data1		TEXT NOT NULL,\
                            data2		TEXT,\
                            data3		TEXT,\
                            data4		TEXT);")
            #need to add index on (ID,type) (date,time,type)
            con.execute("INSERT INTO data (date,time,type,data1) VALUES (?,?,?,?)", (pdate,ptime,"temp1",Temperature) )
            con.execute("INSERT INTO data (date,time,type,data1) VALUES (?,?,?,?)", (pdate,ptime,"humidity",Humidity) )
            con.commit()
            con.close()
        except sqlite.Error, e:
            #log error
            #print "Error %s:" % e.args[0]
            continue
        except IOError, e:
            #log error
            #print "error {0}: {1}".format(e.errorno, e.strerror)
            continue
        finally:
            if (con) : con.close()

if __name__ == '__main__':
    acr_dht11()