# BV4611_I.py
# This is a VT100 128x64 LCD controller, I2C interface
# April 2014 Version 2
# Now uses notSMB
#
from notsmb import notSMB
from time import sleep

# ------------------------------------------------------------------------------
# bv4611 Class
# Initialise example:
# from bv4611_I_v2 import bv4611_I
# disp = bv4611_I(notSMB(1),0x34) OR disp = bv4611_I(notSMB(0),0x34) for older RPi's 
# ------------------------------------------------------------------------------
class bv4611_I:
    """BV4611 126x64 display controller I2C interface"""
    sp = None   # sp is set to communication device by Init()
    def __init__(self,bus,adr):
        self.bus = bus # communication instance
        self.adr = adr
        self.ack = 42 

    def Raw(self,inn,outt):
        return self.bus.i2c(self.adr,inn,outt)
                
    # --------------------------------------------------------------------------
    # Initialise device, this will only return sucess on the first initialisation
    # and can only be done the once for a sucessfull initialisation
    # --------------------------------------------------------------------------
    def Init(self):
        self.bus.i2c(self.adr,[27,91,52,50,69],0) # esc[42E

    # ------------------------------------------------------------------------------
    # generic, sends text to I2C bus.
    # ------------------------------------------------------------------------------
    def Send(self,data):
        #print "DEBUG - sending: "+data 
        self.bus.i2c(self.adr,[ord(c) for c in data],0)
    
    # ------------------------------------------------------------------------------
    # As send but sends esc first
    # ------------------------------------------------------------------------------
    def SendCmd(self,data):
        #print "DEBUG - sending: "+data
        l = [27]
        for c in data:
            l.append(ord(c))
        self.bus.i2c(self.adr,l,0)
            
    # ------------------------------------------------------------------------------
    # for commands that just return ACK,
    # return 0 on sucess
    # ------------------------------------------------------------------------------
    def Wack(self):
        for a in range(0,80):
            sleep(0.005)
            r = self.bus.i2c(self.adr,[],1)
            if r[0] == self.ack:
                #print "DEBUG -- time: "+str(a)
                return 0
        return 1
        
    # ------------------------------------------------------------------------------
    # all commands are preceeded by escape and if init used then must read for
    # the ACK value
    # ------------------------------------------------------------------------------
    def Cmd(self,data):
        self.Raw([],2) # clear any lurking bytes
        self.SendCmd(data)
        self.Wack()
    
    # ------------------------------------------------------------------------------
    # sends text and waits for ack
    # ------------------------------------------------------------------------------
    def Text(self,t):
        self.Send(t)
        self.Raw([],2) # clear any lurking bytes
        self.Cmd("[e") # wait for ack
    
    # ******** CLEARING ***********
    def Cls(self):
        self.Cmd("[2J")
    def ClearLine(self):
        self.Cmd("[2K")
    def ClearLeft(self):
        self.Cmd("[1K")
    def ClearRight(self):
        self.Cmd("[K")

    # ********* SYSTEM ************
    def Lf(self,n): # 1 adds LF any removes
        if n == 1:
            self.Cmd("[?10a")
        else:
            self.Cmd("[?10b")
    def Cursor(self,n): # 1 cursor on any off
        if n == 1:
            self.Cmd("[?25h")
        else:
            self.Cmd("[?25I")
    def Scroll(self,n): # 1 is on any off
        if n == 1:
            self.Cmd("[?26y")
        else:
            self.Cmd("[?26n")
    def Bl(self,n): # bacl light 1 on any off
        if n == 1:
            self.Cmd("[?26h")
        else:
            self.Cmd("[?26I")
    def Crate(self,n): # sets cursor rate
        self.Cmd("["+str(n)+"f")
    
    # ********* CURSOR ************
    def RowCol(self,row,col):
        s = "["+str(row)+';'+str(col)+"H"
        self.Cmd(s)
    def Up(self,n):
        s = "["+str(n)+"A"
        self.Cmd(s)
    def Down(self,n):
        s = "["+str(n)+"B"
        self.Cmd(s)
    def Right(self,n):
        s = "["+str(n)+"C"
        self.Cmd(s)
    def Left(self,n):
        s = "["+str(n)+"D"
        self.Cmd(s)
    def Home(self):
        self.Cmd("[H")
    
    # ********** INFORMATION ********
    def DeviceID(self):
        return self.Raw([27,91,63,51,49,100],2)
    def Version(self):
        return self.Raw([27,91,63,51,49,102],2)
    
    # ********** FONTS **************
    def Font(self,n):
        self.Cmd("("+str(n))
    def Invert(self):
        self.Cmd("[I")
    
    # ********** GRAPHICS ***********
    def Line(self,x0,y0,x1,y1):
        s = "{"+str(x0)+" "+str(y0)+" "+str(x1)+" "+str(y1)+"L"
        self.Cmd(s)
    def Rect(self,x0,y0,x1,y1):
        s = "{"+str(x0)+" "+str(y0)+" "+str(x1)+" "+str(y1)+"R"
        self.Cmd(s)
    def Box(self,x0,y0,x1,y1):
        s = "{"+str(x0)+" "+str(y0)+" "+str(x1)+" "+str(y1)+"F"
        self.Cmd(s)
    def Circle(self,x0,y0,rad):
        s = "{"+str(x0)+" "+str(y0)+" "+str(rad)+"C"
        self.Cmd(s)
    def PutPix(self,x0,y0):
        s = "{"+str(x0)+" "+str(y0)+"P"
        self.Cmd(s)
    def Pix(self,x0,y0):
        s = "{"+str(x0)+" "+str(y0)+"p"
        self.Cmd(s)


