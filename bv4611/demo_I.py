# BV4611 128 x 64 graphics controller
# Version 2 ** change bus to 0 for older RPi 
import sys
from time import sleep 
from notsmb import notSMB
from bv4611_I_v2 import bv4611_I

# ******************************************************************************
# D E M O for BV4611 connected to LCD display
# ******************************************************************************
def main():
    # input com port on start
#    if len(sys.argv) < 2:
#        print "Com port needed as part of input"
#        print "for example: python demo.py '/dev/AMA0'"
#    else:
        # connect to serial port, this can be done in any maner but the 
        # connect in BV4629 is convenient 
        # disp = bv4611_I(notSMB(0),0x34)
        disp = bv4611_I(notSMB(1),0x34)
        disp.Init() # set up ACK important
        print "Boxy background"
        bbg(disp)
        sleep(5)
        print "Font test"
        t2(disp)
        sleep(5)
        print "Clear line test"
        t3(disp)
        sleep(5)
        t4(disp)

# ------------------------------------------------------------------------------
# Boxy background
# ------------------------------------------------------------------------------
def bbg(disp):
    disp.Cls()
    disp.Scroll(0)
    disp.Lf(0) # no LF
    disp.Cursor(0) # no cursor
    disp.RowCol(0,0)
    disp.Send("X")
    disp.RowCol(0,20)
    disp.Send("X")
    disp.RowCol(7,0)
    disp.Send("X")
    disp.RowCol(7,20)
    disp.Send("X")
    disp.Rect(10,10,96,50)
    disp.Line(0,30,127,30)
    disp.Circle(10,30,10)
    disp.Circle(96,30,10)
    disp.PutPix(50,15)
    disp.PutPix(50,16)
    disp.PutPix(51,15)
    disp.PutPix(51,16)
    # wipe out a bit o line using pixel and invert
    disp.Invert()
    for j in range(36,56):
        disp.PutPix(j,10)
    disp.Invert()

# ------------------------------------------------------------------------------
# font test with out of bounds letters on a graphics background 
# ------------------------------------------------------------------------------
def t2(disp):
    disp.Cls()
    disp.Font(1)
    disp.Text("Font number 1")
    disp.RowCol(2,0)
    disp.Font(2)
    disp.Text("Font number 2")
    disp.RowCol(4,0)
    disp.Font(3)
    disp.Text("Font number 3")
    disp.Font(1) # back to font 1

# ------------------------------------------------------------------------------
# clear line tests
# ------------------------------------------------------------------------------
def t3(disp):
    disp.Cls()
    disp.Cursor(1)
    disp.Font(1)
    disp.Text("Top line extends 21 ")
    disp.RowCol(1,0)
    disp.Text("012345678901234567890")
    disp.RowCol(1,10)
    disp.ClearRight()
    sleep(4)
    disp.RowCol(1,0)
    disp.Text("012345678901234567890")
    disp.RowCol(1,10)
    disp.ClearLeft()
    sleep(4)
    disp.RowCol(1,0)
    disp.Text("012345678901234567890")
    disp.RowCol(1,10)
    disp.ClearLine()

# ------------------------------------------------------------------------------
# psudo telematry
# ------------------------------------------------------------------------------
def t4(disp):
    disp.Cls()
    disp.Cursor(0) # off
    disp.Text("Engine #1  319 DegC")
    # DSO
    disp.RowCol(2,0)
    disp.Text("DSO:")
    disp.Rect(25,17,105,21)
    disp.Box(45,18,50,21)
    disp.RowCol(2,18)
    disp.Text("Max")
    # ETU
    disp.RowCol(4,0)
    disp.Text("ETU:")
    disp.Rect(25,33,105,37)
    disp.Box(75,34,80,37)
    disp.RowCol(4,18)
    disp.Text("Max")
    # PK
    disp.RowCol(6,0)
    disp.Text("PK:")
    disp.Rect(25,50,105,60)
    disp.Box(35,57,40,60)
    disp.Box(40,55,45,60)
    disp.Box(45,52,50,60)
    disp.Box(50,52,55,60)
    disp.Box(55,57,60,60)
    disp.Box(60,57,65,60)

if __name__ == "__main__":
    main()
    