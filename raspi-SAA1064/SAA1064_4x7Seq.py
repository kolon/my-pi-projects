# http://s31108.blogspot.co.uk/2012/11/simple-clock-with-i2c-python-and.html

#  simply add this to crontab 
#    * * * * */1 /bin/python SAA1064_4x7Seg.py    	# for every minute with no flashing colon separator
#  or
#     add inside a loop with sleep(.5) and swap the colon status
#

import smbus
import time
import datetime
bus = smbus.SMBus(0)
addr = 0x38
cmd = 0x00
val = 0x27
bus.write_byte_data(addr,cmd,val)

def numToSeg(num):
	retval = 0x00; #default val
	if num==0:
		retval = 0x7E
	elif num==1:
		retval = 0x30
    elif num==2:
		retval = 0x6D
	elif num==3:
		retval = 0x79
	elif num==4:
		retval = 0x33
	elif num==5:
		retval = 0x5B
	elif num==6:
		retval = 0x5F
	elif num==7:
		retval = 0x70
	elif num==8:
		retval = 0x7F
	elif num==9:
		retval = 0x7B
	return retval

now = datetime.datetime.now()
hour_str = now.strftime("%H")
minute_str = now.strftime("%M")
h_0 = int(hour_str[0])
h_1 = int(hour_str[1])
m_0 = int(minute_str[0])
m_1 = int(minute_str[1])

cmd = 0x01
val = numToSeg(h_0)
bus.write_byte_data(addr,cmd,val)

cmd = 0x02
val = numToSeg(h_1)
bus.write_byte_data(addr,cmd,val)

cmd = 0x03
val = numToSeg(m_0) 
bus.write_byte_data(addr,cmd,val)

cmd = 0x04
val = numToSeg(m_1)
bus.write_byte_data(addr,cmd,val)   