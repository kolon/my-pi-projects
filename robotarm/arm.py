# ROBOT ARM CONTROL PROGRAM
# Based on http://www.wikihow.com/Use-a-USB-Robotic-Arm-with-a-Raspberry-Pi-(Maplin)
# Remember to install the PyUSB library before use.
# This script allows you to send a series of commands to your robot arm.
# Example of use: sudo python arm.py ab bc su sd eu ed wu wd go gc lo lf
# 4th January 2014
# Sean Clark (www.seanclark.me.uk)
#
#######################################################################################
# Following notes/additions by John Wood (www.kolon.co.uk) 5th May 2014
# Cloned from Sean Clark https://bitbucket.org/seancuttlefish/raspberry-pi-robotic-arm-controller
# Notes: You may need to install the latest PyUSB on raspberry pi from
#            http://sourceforge.net/projects/pyusb/
#        (Extract and cd to PyUSB, then "sudo python setup.py install")
#         If interested, check out http://www.bobrathbone.com/raspberrypi_robot.htm for a deamon!
# Changes:
#   * added duration to command line (movement duration...movement duration)
#            Example: sudo python arm.py ba 2 bc 2 eu 2 ed 2
#   * load commands from file (use -f filename, one 'command duration' combination per line)
#   * increased usb timeout to something reasonable
#   * multiple commands at once (use -s filename, one command per line (no duration), execute with 'ex duration')
#            (This may break your arm (!) if you specify two movements for the same axis!)
# Todo:
#   * light on/off independant of arm movement (solved by simultaneous movement?)
#    * add "ratio" of directional movement - as the arm motors are not stepper motors, it suffers from
#                gravitational forces (ie going up is harder that going down), thus movements
#                in one direction for duration are not the same distance in the opposite
#                direction for the same duration.  Directional ratios will help but really needs
#                to be use stepper motors.  Also this ratio is needed because of differences between the
#                batteries/power supplies used - the arm requires negative voltage, which it does by grounding
#                between the two groups of D cells in the base.  If both supplies don't provide the same
#                voltage, then opposite directions will move at different speeds, hence a ratio required for
#                compensation).
#
#                I'm unsure how to cater for this, as when the arm lifts a load, it needs to know how heavey the
#                weight is, and re-calculate the ratios accordingly...  (may just skip this for a stepper motor!)
#   * convert duration to "steps" (see http://www.instructables.com/id/Modifications-to-Robot-Arm-for-Opto-Coupler-Feedba/)
#   * stepper motor "reset" position (write movements to file (?) and calculate undo movements) (use deamon?)
#   * interactive keyboard commands
# In fact, not much is left from Sean Clark et al!
#
#

import sys, time, usb.core, usb.util

##################################################################################
#    Error Control

def RobotArmError(Message):
    try:
        ExecuteMovement('st',0)         # at least try to stop the arm from moving
    except:
        print "";
    raise ValueError(Message)

##################################################################################
#    Allocate the name 'RoboArm' to the USB device
RoboArm = usb.core.find(idVendor=0x1267, idProduct=0x000)
if RoboArm is None:
    RobotArmError("Arm not found.  Unplug USB, switch off, switch on, re-plug USB")

##################################################################################
#    Single command

def MoveArm(ArmCmd):
    RoboArm.ctrl_transfer(0x40,6,0x100,0,ArmCmd,100)     #send command to arm

def GetMovementCode(Command):
    if (Command == 'ba'):
        Code = [0,1,0]    # Rotate base anti-clockwise      0b00000001
    elif (Command == 'bc'):
        Code = [0,2,0]    # Rotate base clockwise           0b00000010
    elif (Command == 'sd'):
        Code = [128,0,0]  # Shoulder down                   0b01000000
    elif (Command == 'su'):
        Code = [64,0,0]   # Shoulder up                     0b00100000
    elif (Command == 'ed'):
        Code = [32,0,0]   # Elbow down                      0b00100000
    elif (Command == 'eu'):
        Code = [16,0,0]   # Elbow up                        0b00010000
    elif (Command == 'wd'):
        Code = [8,0,0]    # Wrist down                      0b00001000
    elif (Command == 'wu'):
        Code = [4,0,0]    # Wrist up                        0b00000100
    elif (Command == 'go'):
        Code = [2,0,0]    # Grip open                       0b00000010
    elif (Command == 'gc'):
        Code = [1,0,0]    # Grip close                      0b00000001
    elif (Command == 'lo'):
        Code = [0,0,1]    # Light on                        0b00000001
    elif (Command == 'lf'):
        Code = [0,0,0]    # Light off  (not necessary in single movement mode, as the next movement stops anything)
    elif (Command == 'st'):
        Code = [0,0,0]    # Stop all                        0b00000000
    return Code
        
def ExecuteMovement(Command, Duration):
    if (float(Duration) > 10):             # just make sure we dont ruin our arm!
        RobotArmError("Movement duration " + Duration + " too great - use two separate commands")

    Code = GetMovementCode(Command)        # Convert 'eu' string to code
    MoveArm(Code)                          # Execute movement
    # for "home made stepper motor" (see notes above), this is where you would count number of steps and stop when reached
    time.sleep(float(Duration))            # wait
    Code = GetMovementCode('st')           # Stop movement
    MoveArm(Code)

##################################################################################
#    Simultaneous commands

def get_bit(byteval,idx):
    return ((byteval&(1<<idx))!=0);

def BuildSimultaneousCode(SimultaneousCode,Command):
    #To make sure we don't try to move a motor in two directions at once, we need to
    #compare the various bits of the simultaneous code, and error if two bits are set
    #don't that we dont want
    
    #Build command code
    Code = GetMovementCode(Command)
    SimultaneousCode[0] = SimultaneousCode[0] | Code[0]
    SimultaneousCode[1] = SimultaneousCode[1] | Code[1]
    SimultaneousCode[2] = SimultaneousCode[2] | Code[2]
    
    # go and gc can't be selected
    if (get_bit(SimultaneousCode[0],0) == True) and (get_bit(SimultaneousCode[0],1) == True) :
        RobotArmError("Robot Arm grip can not move in two directions at once")
    # wu and wd can't be selected
    if (get_bit(SimultaneousCode[0],2) == True) and (get_bit(SimultaneousCode[0],3) == True) :
        RobotArmError("Robot Arm wrist can not move in two directions at once")
    # eu and ed can't be selected
    if (get_bit(SimultaneousCode[0],4) == True) and (get_bit(SimultaneousCode[0],5) == True) :
        RobotArmError("Robot Arm elbow can not move in two directions at once")
    # su and sd can't be selected
    if (get_bit(SimultaneousCode[0],6) == True) and (get_bit(SimultaneousCode[0],7) == True) :
        RobotArmError("Robot Arm shoulder can not move in two directions at once")
    # bc and ba can't be selected
    if (get_bit(SimultaneousCode[1],0) == True) and (get_bit(SimultaneousCode[1],1) == True) :
        RobotArmError("Robot Arm base can not move in two directions at once")

    return SimultaneousCode

def ExecuteSimultaneousMovement(SimultaneousCode,Duration):
    if (float(Duration) > 10):
        RobotArmError("Movement duration " + Duration + " too great - use two separate commands")  # to stop arm potentially trying to go further than mechanical allowed
    MoveArm(SimultaneousCode)
    time.sleep(float(Duration))
    ExecuteMovement('st',0)

##################################################################################
#    Interactive Arm Movement

def flush_input():
    try:
        import msvcrt
        while msvcrt.kbhit():
            msvcrt.getch()
    except ImportError:
        import termios
        termios.tcflush(sys.stdin, termios.TCIOFLUSH)
# try to make this as cross-platform as possible (only tested on raspi!)
try:
    from msvcrt import getch
except ImportError:
    def getch():
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

def BasicKeyboardMovement():
    while True:
        flush_input()        # clear input buffer to get most recent keypress
        char = getch()
        #print 'You pressed %s' % char
        Duration = 0.1
        if   (char == '7'): ExecuteMovement('wu',Duration)
        elif (char == '1'): ExecuteMovement('wd',Duration)
        elif (char == '8'): ExecuteMovement('eu',Duration)
        elif (char == '2'): ExecuteMovement('ed',Duration)
        elif (char == '9'): ExecuteMovement('su',Duration)
        elif (char == '3'): ExecuteMovement('sd',Duration)
        elif (char == '4'): ExecuteMovement('ba',Duration)
        elif (char == '6'): ExecuteMovement('bc',Duration)
        elif (char == '0'): ExecuteMovement('go',Duration)
        elif (char == '.'): ExecuteMovement('gc',Duration)
        elif (char == '5'): ExecuteMovement('lo',Duration)
        elif (char == 'x'): break

def SimultaneousKeyboardMovement():
    print "loading..."
    import pygame
    pygame.init()    # is this needed?
    print "go!"
    
    while True:
      time.sleep( 1 )
      events = pygame.event.get()
      print events
      
      if len(events) != 0:
        SimultaneousCode = [0,0,0]
        for event in events:
            print event
            if event.type== pygame.KEYDOWN:
                if event.key== pygame.K_KP_PERIOD: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"gc")
                if event.key== pygame.K_KP0: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"go")
                if event.key== pygame.K_KP1: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"wd")
                if event.key== pygame.K_KP2: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"ed")
                if event.key== pygame.K_KP3: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"sd")
                if event.key== pygame.K_KP4: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"ba")
                if event.key== pygame.K_KP5: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"lo")
                if event.key== pygame.K_KP6: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"bc")
                if event.key== pygame.K_KP7: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"wu")
                if event.key== pygame.K_KP8: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"eu")
                if event.key== pygame.K_KP9: SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,"su")
                if event.key== pygame.K_x: break
        MoveArm(SimultaneousCode)

    ExecuteMovement('st',0)  # stop everything as a last resort

##################################################################################
#    Initial Control co

if (len(sys.argv)) == 1 :
    print "Usage:  (you may need to use sudo!)";
    print "   python arm.py movement duration(seconds) [... movement duration movement duration etc]"
    print "   python arm.py eu 2.5 bc 1.5";
    print "";
    print "   python arm.py -b commands.txt";
    print "      where commands.txt:";
    print "           eu 2.5";
    print "           bc 1.5";
    print "";
    print "   python arm.py -s simultaneous.txt";
    print "      where simultaneous.txt:";
    print "           ed";
    print "           ba";
    print "           ex 2";
    print "";
    print "   python arm.py -ib";
    print "";
    print "   python arm.py -is";
    print "";    
    
elif (str(sys.argv[1])) == '-b' :                    # execute commands in file (singularly)
    f = open(str(sys.argv[2]), 'r')
    for line in f:
        action = line.strip().split(None, 1)
        ExecuteMovement(action[0],float(action[1]))
elif (str(sys.argv[1])) == '-ib' :                    # interactive basic mode (single keypresses)
    BasicKeyboardMovement()
    ExecuteMovement('st',0)                           # Stop all movement (just in case)
elif (str(sys.argv[1])) == '-s' :                    # execute commands in file simultaneously ("ex" = execute)
    f = open(str(sys.argv[2]), 'r')
    SimultaneousCode = [0,0,0]
    for line in f :
        action = line.strip().split(None, 1)
        if (action[0] == 'ex') :                     # execute built up commands
            ExecuteSimultaneousMovement(SimultaneousCode,float(action[1]))
        else :
            SimultaneousCode = BuildSimultaneousCode(SimultaneousCode,action[0])
    ExecuteMovement('st',0)                           # Stop all movement (just in case)
elif (str(sys.argv[1])) == "-is" :                    # interactive enhanced mode (multiple keypresses)
    
    SimultaneousKeyboardMovement()
    ExecuteMovement('st',0)                           # Stop all movement (just in case)  
else :                                                # Parse the command line (must have duration ex: eu 1.2)
    Commands = sys.argv[1:]
    for i in range(1,len(sys.argv),2) :
        Command = str(sys.argv[i])
        Duration = str(sys.argv[i + 1])
        print Command + " " + Duration;
        ExecuteMovement(Command,Duration)
