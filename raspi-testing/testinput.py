try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print("Error importing RPi.GPIO!  This is probably because you need superuser privileges.  You can achieve this by using 'sudo' to run your script")
import time
  
# use Broadcom pin numbering convention
GPIO.setmode(GPIO.BCM)

#specify defaults here - change these to match circuit
PIN_INPUT = 27
PULL_DIR = GPIO.PUD_UP	# default value when switch not pressed - PUD_UP = 3.3v, PUD_DOWN = 0v
# ***** don't edit below here

#test code
GPIO.setup(PIN_INPUT,GPIO.IN, pull_up_down=PULL_DIR)
print "Start: %i" %GPIO.input(PIN_INPUT)

def button_high(PIN):
    print "Button pushed: HIGH"

def button_low(PIN):
    print "Button pushed: LOW"

if PULL_DIR == GPIO.PUD_DOWN:
    GPIO.add_event_detect(PIN_INPUT, GPIO.RISING, callback=button_high, bouncetime=300)	# add rising edge detection on a channel
else:
    GPIO.add_event_detect(PIN_INPUT, GPIO.FALLING, callback=button_low, bouncetime=300)	# or a falling one

while True:
    input("Press ENTER to exit...")
    #time.sleep(0.01)

GPIO.remove_event_detect(PIN_INPUT)
GPIO.cleanup()
