try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print("Error importing RPi.GPIO!  This is probably because you need superuser privileges.  You can achieve this by using 'sudo' to run your script")
import time
  
# use Broadcom pin numbering convention
GPIO.setmode(GPIO.BCM)

#specify defaults here - change these to match circuit
PIN_OUTPUT = 4
# ***** don't edit below here

#test code
GPIO.setup(PIN_OUTPUT,GPIO.OUT)
print "Start"

BRIGHT = GPIO.PWM(PIN_OUTPUT,100)
BRIGHT.start(1)

while True:
    print "Brightening..."
    for i in range(100):
        BRIGHT.ChangeDutyCycle(i)
        time.sleep(0.1)

    print "Dimming..."
    for i in range(100):
        BRIGHT.ChangeDutyCycle(100-i)
        time.sleep(0.1)

BRIGHT.stop

GPIO.cleanup()
